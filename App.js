import {NavigationContainer} from '@react-navigation/native';
// Must Navigation V.5
import {createStackNavigator} from '@react-navigation/stack';
import React from 'react';
import HomeScreen from 'screens/Home';

const Stack = createStackNavigator();

function StackNav(props) {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home" headerMode={'none'}>
        <Stack.Screen name="Home" component={HomeScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default StackNav;
