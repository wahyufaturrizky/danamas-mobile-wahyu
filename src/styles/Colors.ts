export enum ColorBaseEnum {
  black = '#000',
  white = '#FFF',
  whiteOpacity = 'rgba(255, 255, 255, 0.5)',
  blackOpacity = 'rgba(0, 0, 0, 0.5)',
}

export enum ColorBaseTransparentEnum {
  whiteTransparent = '#FFFFFF00',
}

export enum ColorPrimaryEnum {
  cream = '#FCF0DE',
  blue = '#205C8C',
  orange = '#FD6542',
  orangeOpacity = 'rgba(232, 119, 34, 0.5)',
  red = '#F70161',
  green = 'rgb(11,104,80)',
  greenDark = '#183028',
  yellow = 'rgb(252,173,16)',
  redLight = '#FEF8FA',
  grey = '#F8F7F5'
}

export enum ColorBaseGrayEnum {
  gray100 = '#FAFAFA',
  gray200 = '#F5F5F5',
  gray300 = '#EBEBEB',
  gray400 = '#BDBDBD',
  gray500 = '#9E9E9E',
  gray600 = '#555555',
  gray700 = '#212121'
}

export enum ColorSemanticInfoEnum {
  darker = '#09509D',
  dark = '#2371C6',
  default = '#4393EA',
  light = '#D3E5FA',
  lighter = '#F0F7FF'
}
export enum ColorSemanticPositiveEnum {
  darker = '#0B6732',
  dark = '#19954D',
  default = '#27AE60',
  light = '#BEF0D3',
  lighter = '#E3F6EB'
}

export enum ColorSemanticWarningEnum {
  darker = '#904E01',
  dark = '#D97A0B',
  default = '#FF8A00',
  light = '#FFE7CA',
  lighter = '#FFF8EF'
}

export enum ColorSemanticDangerEnum {
  darker = '#76102B',
  dark = '#AF2631',
  default = '#CC3434',
  light = '#EF9784',
  lighter = '#FCE3D6'
}
