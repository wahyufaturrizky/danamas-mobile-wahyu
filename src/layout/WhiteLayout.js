import React from 'react';
import {View} from 'react-native';
import styled from 'styled-components';
import {ColorBaseEnum, ColorBaseGrayEnum} from 'styles/Colors';
import {BorderRadiusEnum, MarginEnum, PaddingEnum} from 'styles/Spacer';

const WhiteLayoutStyled = styled(View)`
  flex: 1;
  padding: ${MarginEnum['3x']} ${MarginEnum['4x']};
  border-radius: ${(props: {borderRadius?: BorderRadiusEnum}) =>
    props.borderRadius || BorderRadiusEnum['0x']};
`;

const WhiteLayout = ({children, borderRadius, backgroundColor}) => (
  <View
    style={{
      backgroundColor: backgroundColor || ColorBaseEnum.white,
      flex: 1,
      paddingVertical: PaddingEnum['3x'],
      paddingHorizontal: PaddingEnum['4x'],
      borderRadius: borderRadius || BorderRadiusEnum['0x'],
    }}>
    {children}
  </View>
);

export default WhiteLayout;
