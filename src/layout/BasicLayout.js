import React from 'react';
import {View} from 'react-native';
import {MarginEnum} from 'styles/Spacer';
import {ScrollView} from 'react-native-gesture-handler';

export const BasicLayout = (props) =>
  props.isWithoutScroll ? (
    <View
      style={{
        marginBottom: props.marginBottom,
        marginTop: props.marginTop,
        flex: 1,
        marginLeft: MarginEnum['4x'],
        marginRight: MarginEnum['4x'],
      }}>
      {props.children}
    </View>
  ) : (
    <ScrollView
      refreshControl={props.refreshControl}
      scrollIndicatorInsets={{right: Number.MIN_VALUE}}
      contentContainerStyle={{
        flexGrow: props.disableGrow ? 0 : 1,
        justifyContent: 'center',
        backgroundColor: props.backgroundColor,
      }}>
      <View
        style={{
          marginBottom: props.marginBottom,
          marginTop: props.marginTop,

          flex: 1,
          marginLeft: MarginEnum['4x'],
          marginRight: MarginEnum['4x'],
        }}>
        {props.children}
      </View>
    </ScrollView>
  );
