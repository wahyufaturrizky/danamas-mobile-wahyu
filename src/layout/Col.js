import React from 'react';
import {View} from 'react-native';

export const Col = (props) => {
  return (
    <View
      style={{
        flex: props.size,
        marginLeft: props.marginLeft,
        justifyContent: props.justifyContent,
        alignItems: props.alignItems,
        marginBottom: props.marginBottom,
        alignSelf: props.alignSelf,
        paddingLeft: props.paddingLeft,
        paddingRight: props.paddingRight,
        borderLeftWidth: props.borderLeftWidth,
        borderLeftColor: props.borderLeftColor,
        backgroundColor: props.backgroundColor,
      }}>
      {props.children}
    </View>
  );
};
