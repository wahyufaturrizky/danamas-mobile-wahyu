import {BasicLayout} from 'layout/BasicLayout';
import React, {Fragment, useEffect, useState} from 'react';
import {MarginEnum} from 'styles/Spacer';
import {Text, StyleSheet, View, Picker} from 'react-native';
import {Calendar} from 'react-native-calendars';
import SplashScreen from 'react-native-splash-screen';
import moment from 'moment';

const Home = () => {
  const dataYears = [
    '2014',
    '2015',
    '2016',
    '2017',
    '2018',
    '2019',
    '2020',
    '2021',
  ];
  const dataMonth = [
    'January',
    'February',
    'March',
    'April',
    'Mei',
    'June',
    'July',
    'Augustus',
    'September',
    'October',
    'November',
    'December',
  ];
  const [year, setYear] = useState('2021');
  const [month, setMonth] = useState('December');
  useEffect(() => {
    SplashScreen.hide();
  }, []);

  return (
    <>
      <BasicLayout>
        <Fragment>
          <View style={{marginTop: MarginEnum['4x']}}>
            <Text style={styles.text}>Start Date</Text>
            <View style={{flexDirection: 'row', display: 'flex'}}>
              <Picker
                selectedValue={year}
                style={{height: 50, width: 150}}
                onValueChange={(itemValue, itemIndex) => setYear(itemValue)}>
                {dataYears.map((data, index) => (
                  <Picker.Item key={index} label={data} value={data} />
                ))}
              </Picker>
              <Picker
                selectedValue={month}
                style={{height: 50, width: 150}}
                onValueChange={(itemValue, itemIndex) => setMonth(itemValue)}>
                {dataMonth.map((data, index) => (
                  <Picker.Item key={index} label={data} value={data} />
                ))}
              </Picker>
            </View>
            <Calendar
              current={moment(new Date()).format('YYYY-MM-DD')}
              minDate={moment().startOf('month').format('YYYY-MM-DD')}
              markingType={'period'}
              markedDates={{
                [moment(new Date()).format('YYYY-MM-DD')]: {
                  startingDay: true,
                  color: '#50cebb',
                  textColor: 'white',
                  endingDay: true,
                },
                '2021-12-05': {
                  startingDay: true,
                  color: '#50cebb',
                  textColor: 'white',
                },
                '2021-12-06': {
                  color: 'rgba(249, 48, 114, 0.5)',
                  textColor: 'white',
                },
                '2021-12-07': {
                  color: 'rgba(249, 48, 114, 0.5)',
                  textColor: 'white',
                },
                '2021-12-08': {
                  color: 'rgba(249, 48, 114, 0.5)',
                  textColor: 'white',
                },
                '2021-12-09': {
                  color: 'rgba(249, 48, 114, 0.5)',
                  textColor: 'white',
                },
                '2021-12-10': {
                  color: 'rgba(249, 48, 114, 0.5)',
                  textColor: 'white',
                },
                '2021-12-11': {
                  color: 'rgba(249, 48, 114, 0.5)',
                  textColor: 'white',
                },
                '2021-12-12': {
                  color: 'rgba(249, 48, 114, 0.5)',
                  textColor: 'white',
                },
                '2021-12-13': {
                  endingDay: true,
                  color: '#FA355E',
                  textColor: 'white',
                },
              }}
              disabledDaysIndexes={[0, 6]}
              theme={{
                textSectionTitleDisabledColor: 'grey',
                textSectionTitleColor: '#00BBF2',
              }}
            />
          </View>
        </Fragment>
      </BasicLayout>
    </>
  );
};

const styles = StyleSheet.create({
  text: {
    textAlign: 'left',
    padding: 10,
    backgroundColor: 'white',
    fontSize: 16,
  },
});

export default Home;
